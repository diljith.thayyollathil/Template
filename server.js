var express = require('express');
var path = require('path');
var app = express();
var hbs = require('hbs');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var constant = require('./config/constant');

const SECRET_KEY_BASE = constant.SECRET_KEY_BASE;
const MONGO_URL = constant.MONGO_URL;
const PORT = constant.PORT;

var auth = require('./auth');
var connection = mongoose.connect(MONGO_URL);

app.use(express.static(__dirname + '/static'));

app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json({ limit: '25mb' })); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true, limit: '25mb' }));


//setting static directory and view engine
app.set('view engine', 'hbs');
app.set('views', __dirname + '/auth/views');

// set up the http server for
var http = require('http').Server(app);

var auth = auth.initialize(app, http);

//listening http server to port
http.listen(PORT, function () {
  console.log('Running at http://127.0.0.1:8080');
});
