module.exports = {
  SECRET_KEY_BASE: 'ThisIsTheSecret',
  MONGO_URL: 'mongodb://localhost/login',
  PORT: 8080,
}
